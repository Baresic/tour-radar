//Function for adding three dots
function add3Dots(string, limit){
  var dots = "...";
  if(string.length > limit){
    string = string.substring(0,limit) + dots;
  }
    return string;
}

$.fn.scrollView = function () {
    return this.each(function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top - 10
        }, 300);
    });
}


//Variables
var left;
var right;
var nizResponse = [];
var $handle = [];
var $li = $('.list');
var skipSlider = document.getElementById('range');
var scroll = $(window).scrollTop();

//main data call function
function getResponse(sortBy) {

    $('.list').find('li').remove();

    //Ajax Calls
    $.ajax({
        url: 'data.JSON',
        dataType:'JSON',
        // async: false,
        success:function(data){

          //Sort function
            data.sort(function(a, b) {
                var propertyA = a[sortBy];
                var propertyB = b[sortBy];
                if(typeof a[sortBy] === 'string') {
                    propertyA = a[sortBy].toUpperCase(); // ignore upper and lowercase
                    propertyB = b[sortBy].toUpperCase(); // ignore upper and lowercase
                }
                if (propertyA < propertyB) {
                    return -1;
                }
                if (propertyA > propertyB) {
                    return 1;
                }
                return 0;
            });

            //looping thrue main data object
            for (var i = 0; i < data.length; i++) {
                nizResponse.push(data[i]);
                $handle.push(data[i].length);
                //arrays for handling data
                var pricesArray = [];
                var discountArray = [];

                // Price and discount
                for (var j = 0; j < data[i].dates.length; j++) {

                  // price condition if price array is empty grab first iteration
                    if(pricesArray.length === 0) {
                        pricesArray.push(data[i].dates[j]);
                    }

                    //make unique with lowest price array
                    else if (data[i].dates[j].usd <= pricesArray[0].usd) {
                        pricesArray.push(data[i].dates[j]);
                    }
                  //discount
                    if(data[i].dates[j].discount) {
                        if(discountArray.length == 0) {
                            discountArray.push(data[i].dates[j].discount);
                        }
                        else if(data[i].dates[j].discount > discountArray){
                            discountArray = [];
                            discountArray.push(data[i].dates[j].discount)
                        }
                    }
                    else {
                        discountArray.push('--');
                    }

                }

               //Stars
               var floorStar = Math.floor(data[i].rating);
               var allStars = '';
               for (var j = 0; j < floorStar; j ++) {
                   allStars += '<div class="full"></div>';
               }
               if (data[i].rating > floorStar) {
                   allStars += '<div class="half"></div>'
               }

               //hadles range nonuislider values
               left = Math.min.apply(null,$handle);
               right = Math.max.apply(null,$handle);



                //appending html to dom
                $('.list').find('ul').append('' +
                    '<li id="listEl-' + i + '" data-length="' + data[i].length + '">' +

                    '<a class="list--image" href="'+ data[i].url +'">' +
                    '<img src="' + data[i].images[0].url + '" alt="' + data[i].images[0].name + '">' +
                    '<div class="discount"><div class="percent">-' + discountArray[0] + '</div></div>' +
                    '<div class="heart"></div>' +
                    '<div class="rating">' +
                    '<div class="stars" data-rating="' + data[i].rating + '">' + allStars + '</div>' +
                    '<span class="review">' + data[i].reviews + " reviews" + '</span>' +
                    '</div>' +
                    '</a>' +

                    '<div class="list--middle">' +
                    '<a class="title-link" href="' + data[i].url + '">' +
                    '<h4 class="title">' + data[i].name + '</h4>' +
                    '</a>' +
                    '<blockquote class="description">' + add3Dots(data[i].description, 100) + '</blockquote>' +
                    '<dl class="values">' +
                    '<dt>days</dt>' +
                    '<dd class="len">' + data[i].length + '</dd>' +
                    '<dt>destinations</dt>' +
                    '<dd class="dest">' + data[i].cities.length + '</dd>' +
                    '<dt>starts/ ends in</dt>' +
                    '<dd class="start">' + data[i].cities[0].name + ' / ' + data[i].cities[data[i].cities.length - 1].name + '</dd>' +
                    '<dt>operator</dt>' +
                    '<dd><a class="operator" href="' + data[i].website + '" target="_blank">' + data[i].operator_name + '</a></dd>' +
                    '</dl>' +
                    '</div>' +

                    '<div class="list--offer">' +
                    '<h4>Total price from</h4>' +
                    '<div class="price">' +
                    '<span class="cur">$</span>' +
                    '<span class="sum">' + pricesArray[0].usd +'</span>' +
                    '<span class="ico"></span>' +
                    '</div>' +
                    '<table>' +
                    '<tbody>' +
                    '<tr>' +
                    '<td class="first-date">' + moment(pricesArray[0].start).format('YYYY MMM DD') + '</td>' +
                    '<td class="seats">' + '</span>' + pricesArray[1].availability + '</span>' + " seats left" + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td class="second-date">' + moment(pricesArray[1].start).format('YYYY MMM DD') + '</td>' +
                    '<td class="seats">' + '</span>' + pricesArray[1].availability + '</span>' + " seats left" + '</td>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table>' +
                    '<a class="blue" href="'+ data[i].url +'">View More</a>' +
                    '</div>' +

                    '</li>'
                );
            }

            //NonUi Slider
            noUiSlider.create(skipSlider, {
                  start: [ left, right ],
                  step: 1,
                  range: {
                    'min': left,
                    'max': right
                  },
                  connect: true,
                  format: {
                  from: function(value) {
                          return parseInt(value);
                      },
                  to: function(value) {
                          return parseInt(value);
                      }
                  }
                },true);

              var skipValues = [
                document.getElementById('range-value-lower'),
                document.getElementById('range-value-upper')
              ];
              //update slider numbers
              skipSlider.noUiSlider.on('update', function( values, handle ) {
                skipValues[handle].innerHTML = values[handle];
              });

              //Event after handles stop
              skipSlider.noUiSlider.on('set', function(values, handle){

              //Values of handles array
              var arr = skipSlider.noUiSlider.get();

              //Slider filtering logic
              function showItems( minLength, maxLength) {
                var child = $li.find('li');
                child.each(function(index, el) {
                  $(this).addClass('hidden').filter(function() {
                      var length = parseInt( $(this).data("length") );
                      // console.log(length);
                      return length >= minLength && length <= maxLength;
                  }).removeClass('hidden');
                });
              }
              showItems(arr[0], arr[1]);
            });

      },
      error:function(){
        console.log('there was error with fetching data');
      }
    });
}
//printing html and sorting by name
getResponse('name');


//DOCUMENT READY TIME
$(document).ready(function () {
  //select handling for sorting
    $('#sorter').on('change', function() {
        getResponse(this.value);
        skipSlider.noUiSlider.destroy();
        $(this).scrollView();
    });

    $('#mob').on('change', function() {
        getResponse(this.value);
        skipSlider.noUiSlider.destroy();
        $(this).scrollView();
    });
    //Date picker
     var picker = new Pikaday({ field: document.getElementById('datepicker') });

});
